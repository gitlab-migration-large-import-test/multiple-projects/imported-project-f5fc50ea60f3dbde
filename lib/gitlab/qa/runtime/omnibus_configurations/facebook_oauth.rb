# frozen_string_literal: true

module Gitlab
  module QA
    module Runtime
      module OmnibusConfigurations
        class FacebookOauth < Default
          def configuration
            Runtime::Env.require_github_oauth_environment!

            <<~OMNIBUS
              gitlab_rails['omniauth_enabled'] = true;
              gitlab_rails['omniauth_allow_single_sign_on'] = [facebook'];
              gitlab_rails['omniauth_block_auto_created_users'] = false;
              gitlab_rails['omniauth_providers'] = [
                {
                  name: 'facebook',
                  app_id: '$QA_FACEBOOK_OAUTH_APP_ID',
                  app_secret: '$QA_FACEBOOK_OAUTH_APP_SECRET',
                  verify_ssl: false
                }
              ];
              letsencrypt['enable'] = false;
              external_url '<%= gitlab.address %>';
            OMNIBUS
          end
        end
      end
    end
  end
end
